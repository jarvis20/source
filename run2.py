from pexpect import pxssh
s = pxssh.pxssh()
if not s.login ('localhost', 'pi', 'raspberry'):
    print "SSH session failed on login."
    print str(s)
else:
    print "SSH session login successful"
    s.sendline ('uptime')
    s.prompt()         # match the prompt
    #print s.before     # print everything before the prompt.
    per=s.before
    print per
    print per,per[40:42]            
    s.logout()
    print
#We can also execute multiple command like this:
s.sendline ('uptime;df -h')
