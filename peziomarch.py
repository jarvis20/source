import RPi.GPIO as GPIO 
import time 

GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT) 
GPIO.setup(15, GPIO.OUT) 


d3 = 155.56
g3 = 196.00
a4 = 466.16
d4 = 293.66
d4s= 311.13
f3 = 185.00
r = 1
p = GPIO.PWM(15, 100)


def Blink(numTimes, speed):
    for i in range(0,numTimes): 
        print "Iteration " + str(i+1) 
        GPIO.output(7, True) 
        GPIO.output(15, True) 
        time.sleep(speed) ## Wait
        p.start(100)             # start the PWM on 100  percent duty cycle  
        p.ChangeDutyCycle(90)   # change the duty cycle to 90%  
        p.ChangeFrequency(g3)  # change the frequency to 261 Hz (floats also work)  
        time.sleep(speed) ## Wait
        p.ChangeFrequency(g3)  # change the frequency to 294 Hz (floats also work)  
        time.sleep(speed) ## Wait
        p.ChangeFrequency(g3)   
        time.sleep(speed) ## Wait
        p.ChangeFrequency(d3)  
        time.sleep(speed) ## Wait
        p.ChangeFrequency(a4)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(g3)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(d3)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(a4)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(g3)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(d4)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(d4)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(d4)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(d4s)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(a4)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(a4)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(f3)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(d3)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(a4)    
        time.sleep(speed) ## Wait
        p.ChangeFrequency(g3)    
        time.sleep(speed) ## Wait
      
        p.ChangeFrequency(r)  
        time.sleep(speed) ## Wait
        p.stop()                # stop the PWM output  

    print "Done" ## When loop is complete, print "Done"
    GPIO.cleanup()

iterations = 2
speed = .5

Blink(int(iterations),float(speed))

