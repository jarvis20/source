import RPi.GPIO as GPIO
import time
import smtplib
import picamera
from pexpect import pxssh
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import serial
import RPi.GPIO as GPIO   #import the GPIO library
import time               #import the time library
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(16, GPIO.IN)         #Read output from PIR motion sensor

s = pxssh.pxssh()
match="1"
ser = serial.Serial('/dev/ttyUSB0', 9600)
ser.baudrate = 9600
class Buzzer(object):
       def __init__(self):
                  
                self.buzzer_pin = 29 #set to GPIO pin 5
                GPIO.setup(self.buzzer_pin, GPIO.IN)
                GPIO.setup(self.buzzer_pin, GPIO.OUT)

       def __del__(self):
                class_name = self.__class__.__name__
  
       def buzz(self,pitch, duration):

               if(pitch==0):
                     time.sleep(duration)
                     return
               period = 1.0 / pitch     #in physics, the period (sec/cyc) is the inverse of the frequency (cyc/sec)
               delay = period / 2     #calcuate the time for half of the wave  
               cycles = int(duration * pitch)   #the number of waves to produce is the duration times the frequency

               for i in range(cycles):   
                     GPIO.output(self.buzzer_pin, True)   #set pin 18 to high
                     time.sleep(delay)    #wait with pin 18 high
                     GPIO.output(self.buzzer_pin, False)    #set pin 18 to low
                     time.sleep(delay)    #wait with pin 18 low

       def play(self, tune):
              
              GPIO.setup(self.buzzer_pin, GPIO.OUT)
              x=0
              if(tune==5):
                     pitches=[500]
                     duration=[0.3]
                     for p in pitches:
                           self.buzz(p, duration[x])  
                           time.sleep(duration[x] *0.5)
                           x+=1
              elif(tune==1):
                     pitches=[262,294,330,349,392,440,494,523, 587, 659,698,784,880,988,1047]
                     duration=0.1
                     for p in pitches:
                           self.buzz(p, duration)  
                           time.sleep(duration *0.5)
                           for p in reversed(pitches):
                               self.buzz(p, duration)
                               time.sleep(duration *0.5)

              elif(tune==2):     
                  pitches=[262,330,392,523,1047]
                  duration=[0.2,0.2,0.2,0.2,0.2,0,5]
                  for p in pitches:
                     self.buzz(p, duration[x])  
                     time.sleep(duration[x] *0.5)
                     x+=1
              elif(tune==3):
                  pitches=[392,294,0,392,294,0,392,0,392,392,392,0,1047,262]
                  duration=[0.2,0.2,0.2,0.2,0.2,0.2,0.1,0.1,0.1,0.1,0.1,0.1,0.8,0.4]
                  for p in pitches:
                         self.buzz(p, duration[x])  
                         time.sleep(duration[x] *0.5)
                         x+=1

              elif(tune==4):
                  pitches=[1047, 988,659]
                  duration=[0.1,0.1,0.2]
                  for p in pitches:
                    self.buzz(p, duration[x])  
                    time.sleep(duration[x] *0.5)
                    x+=1







#here = 1

#Motion sensor start
while True:
       i=GPIO.input(16)
       
       if i==0:                 #When output from motion sensor is LOW
            
            print "No intruders",i
            time.sleep(0.1)
       elif i==1:               #When output from motion sensor is HIGH
            print "Intruder detected",i
            #here = here+1
            
#area photo 
            camera= picamera.PiCamera()
            camera.capture('/home/pi/Desktop/pic.jpg')
            #camera.capture('/home/pi/Desktop/pic%d.jpg' %here)
            camera.close()
#area photo

#email start
            fromaddr = "projecthere112@gmail.com"
            toaddr = "igotbestskill@gmail.com","drc247@gmail.com"
            msg = MIMEMultipart()
            msg['From'] = fromaddr
            msg['To'] = ", ".join(toaddr)
            msg['Subject'] = "Entry way Alert"

            body = "Near your front door."
 
            msg.attach(MIMEText(body, 'plain'))
            filename = "demo.jpg"
            attachment = open("/home/pi/Desktop/pic.jpg", "rb")
            part = MIMEBase('application', 'octet-stream')

            part.set_payload((attachment).read())

            encoders.encode_base64(part)

            part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
 
            msg.attach(part)
 
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(fromaddr, "185185here")
            text = msg.as_string()
            server.sendmail(fromaddr, toaddr, text)
            server.quit()
            #time.sleep(10)
            
            #email end

            #keypad begin
            ser = serial.Serial('/dev/ttyUSB0', 9600)
            ser.baudrate = 9600
            #read_ser1 = ser.readline()
            #print(read_ser1)
            print "enter password"
            read_key = ser.readline()
            print(read_key)   
            if (read_key != "6666"):
                    read_message = ser.readline()
                    print(read_message)
                    buzzer = Buzzer()
                    buzzer.play(4)  #Failed buzzer sound
                    
              
            elif (read_key == "6666"):
                    read_message = ser.readline()
                    key = read_message
                    print(read_message)
                    buzzer = Buzzer()
                    buzzer.play(5) #passed buzzer sound
                    
            #break 
            #keypad end






            
            #ssh/Face recognition
        
            if not s.login ('10.113.38.15', 'pi', 'raspberry'):#192.168.43.105
                print "SSH session failed on login."
                print str(s)
            else:
                print "SSH session login successful"
                s.sendline ('python /home/pi/pi-detector/scripts/face.py;python /home/pi/pi-detector/scripts/facematch.py -i \'/home/pi/pi-detector/scripts/image.jpg\' -c \'home\'')
                s.prompt()         # match the prompt
                per=s.before
                print per
                match=per[357:373]	            #Identity matched
                print match
            if (match == "Identity matched"):
                   buzzer = Buzzer()
                   buzzer.play(5)
            else:
                   buzzer = Buzzer()
                   buzzer.play(4)
                   # if not s.login ('10.113.38.15', 'pi', 'raspberry'):#192.168.43.105
                   #  print "SSH session failed on login."
                   #  print str(s)
                   #else:
                   #  print "SSH session login successful"
                   s.prompt() # match the prompt
                   s.sendline ('python /home/pi/pi-detector/scripts/facesend.py')
                   per=s.before
                   print "face sent"
                   s.logout()
                   time.sleep(10)
                       

       
#We can also execute multiple command like this:
#ssh end
