from pexpect import pxssh
s = pxssh.pxssh()
if not s.login ('10.113.38.15', 'pi', 'raspberry'):#192.168.43.105
    print "SSH session failed on login."
    print str(s)
else:
    print "SSH session login successful"
    #s.sendline ('python faces.py')
    s.sendline ('python /home/pi/pi-detector/scripts/face.py;python /home/pi/pi-detector/scripts/facematch.py -i \'/home/pi/pi-detector/scripts/image.jpg\' -c \'home\'')
    s.prompt()         # match the prompt
    print s.before     # print everything before the prompt.
    s.logout()
    
#We can also execute multiple command like this:
#s.sendline ('uptime;df -h')
