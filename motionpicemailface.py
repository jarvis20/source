import RPi.GPIO as GPIO
import time
import smtplib
import picamera
from pexpect import pxssh
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
 
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(16, GPIO.IN)         #Read output from PIR motion sensor

#here = 1

#Motion sensor start
while True:
       i=GPIO.input(16)
       if i==0:                 #When output from motion sensor is LOW
            print "No intruders",i
            time.sleep(0.1)
       elif i==1:               #When output from motion sensor is HIGH
            print "Intruder detected",i
            #here = here+1
            
#area photo 
            camera= picamera.PiCamera()
            camera.capture('/home/pi/Desktop/pic.jpg')
            #camera.capture('/home/pi/Desktop/pic%d.jpg' %here)
            camera.close()
#area photo

#email start
            fromaddr = "projecthere112@gmail.com"
            toaddr = "igotbestskill@gmail.com","drc247@gmail.com"
            msg = MIMEMultipart()
            msg['From'] = fromaddr
            msg['To'] = ", ".join(toaddr)
            msg['Subject'] = "Entry way Alert"

            body = "Near your front door."
 
            msg.attach(MIMEText(body, 'plain'))
            filename = "demo.jpg"
            attachment = open("/home/pi/Desktop/pic.jpg", "rb")
            part = MIMEBase('application', 'octet-stream')

            part.set_payload((attachment).read())

            encoders.encode_base64(part)

            part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
 
            msg.attach(part)
 
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(fromaddr, "185185here")
            text = msg.as_string()
            server.sendmail(fromaddr, toaddr, text)
            server.quit()
            #time.sleep(10)
#email end
#ssh/Face recognition
            s = pxssh.pxssh()
            if not s.login ('10.113.38.15', 'pi', 'raspberry'):#192.168.43.105
                print "SSH session failed on login."
                print str(s)
            else:
                print "SSH session login successful"
                s.sendline ('python /home/pi/pi-detector/scripts/face.py;python /home/pi/pi-detector/scripts/facematch.py -i \'/home/pi/pi-detector/scripts/image.jpg\' -c \'home\'')
                s.prompt()         # match the prompt
                per=s.before
                print per
                match=per[357:373]	            #Identity matched
                print match
                s.logout()
                print
#We can also execute multiple command like this:            
#ssh end